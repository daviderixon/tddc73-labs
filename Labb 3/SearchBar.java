package com.example.odavi.tddc73labb3;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public class SearchBar extends LinearLayout {
    private String TAG = SearchBar.class.getSimpleName();
    Context c;
    EditText inputField;
    ListView content;
    String url = "";
    String SearchTerm = "";
    ArrayList<String> Search;
    ArrayAdapter Adapter;
    PopupWindow PW;
    int Max;
    int urlID = 0;

    LinearLayout layout;

    public SearchBar(Context con, int MaxResults){
        super(con);
        this.c = con;
        Max = MaxResults;
        init();
    }
    private void init(){
        this.setOrientation(LinearLayout.VERTICAL);
        this.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        inputField = new EditText(c);
        content = new ListView(c);
        inputField.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        addView(inputField);

        PW = new PopupWindow(c);
        layout = new LinearLayout(c);
        layout.setOrientation(LinearLayout.VERTICAL);

        Search = new ArrayList<>();
        Adapter = new SearchResultAdapter(c,android.R.layout.simple_list_item_1,Search);
        content.setAdapter(Adapter);
        layout.addView(content);
        PW.setContentView(layout);

        inputField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length() != 0){
                    SearchTerm = s.toString();
                    url = "http://andla.pythonanywhere.com/getnames/"+urlID+"/" + SearchTerm;
                    new PerformSearch().execute();

                }
                else{
                    Search.clear();
                    PW.dismiss();
                    Adapter.notifyDataSetChanged();
                }

            }
            @Override
            public void afterTextChanged(Editable s) {

            }

        });
        content.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                inputField.setText(Search.get(arg2));
                inputField.setSelection(inputField.getText().length());
            }

        });

    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class PerformSearch extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... arg0) {
            HttpReq sh = new HttpReq();

            // Making a request to url and getting response
            String jsonStr = sh.Request(url);

            return jsonStr;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    JSONObject jsonObj = new JSONObject(result);
                    String id =  jsonObj.getString("id");
                    // Getting JSON Array node
                    JSONArray names = jsonObj.getJSONArray("result");
                    Search.clear();
                    // looping through All Names
                    if(id.equals(Integer.toString(urlID))) {
                        for (int i = 0; i < Math.min(Max, names.length()); i++) {
                            Search.add((String) names.get(i));
                        }
                    }

                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());

                }
            } else {

                Log.e(TAG, "Couldn't get json from server.");

            }
            Adapter.notifyDataSetChanged();
            if(Search.size() == 1 && Search.get(0).equals(SearchTerm)){
                PW.dismiss();
            }
            else{
                PW.showAtLocation(layout, Gravity.NO_GRAVITY, 50, 250);
                PW.update(50, inputField.getHeight()*3, inputField.getWidth(), 55*Search.size());
            }
            urlID++;


        }

    }
    private class PopUp extends View{


        public String text;
        public PopUp(Context context, String s) {
            super(context);
            text = s;
        }

        @Override
        protected void onDraw(Canvas canvas){
            Paint style = new Paint();
            style.setColor(Color.BLACK);
            style.setTextSize(38);
            canvas.drawARGB(100,150,150,150);
            canvas.drawText(text, 20,40,style);


        }
        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

            int desiredWidth = 200;
            int desiredHeight = 50;

            int widthMode = MeasureSpec.getMode(widthMeasureSpec);
            int widthSize = MeasureSpec.getSize(widthMeasureSpec);
            int heightMode = MeasureSpec.getMode(heightMeasureSpec);
            int heightSize = MeasureSpec.getSize(heightMeasureSpec);

            int width;
            int height;

            //Measure Width
            if (widthMode == MeasureSpec.EXACTLY) {
                //Must be this size
                width = widthSize;
            } else if (widthMode == MeasureSpec.AT_MOST) {
                //Can't be bigger than...
                width = Math.min(desiredWidth, widthSize);
            } else {
                //Be whatever you want
                width = desiredWidth;
            }

            //Measure Height
            if (heightMode == MeasureSpec.EXACTLY) {
                //Must be this size
                height = heightSize;
            } else if (heightMode == MeasureSpec.AT_MOST) {
                //Can't be bigger than...
                height = Math.min(desiredHeight, heightSize);
            } else {
                //Be whatever you want
                height = desiredHeight;
            }

            //MUST CALL THIS
            setMeasuredDimension(width, height);
        }
        public String getContent(){
            return text;
        }

    }
    private class SearchResultAdapter extends ArrayAdapter<String>{

        public SearchResultAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
        }
        @Override
        public View getView(int pos, View convertView, ViewGroup parent){

            PopUp P = new PopUp(c,getItem(pos));
            convertView = P;


            return convertView;
        }
    }

}
