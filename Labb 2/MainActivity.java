package com.example.odavi.tddc73_labb2;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import static android.util.Half.NaN;

public class MainActivity extends AppCompatActivity {
    MyExpandableListAdapter adapter;
    ExpandableListView Exp;
    EditText TV;
    View currentmarkerad;
    Boolean B = true;

    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Exp = (ExpandableListView) this.<View>findViewById(R.id.EXP);
        TV = (EditText) this.<View>findViewById(R.id.TopText);
        TV.setText("/");
        prepData();
        adapter = new MyExpandableListAdapter(this,listDataHeader, listDataChild);


        Exp.setAdapter(adapter);

        TV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Boolean ShouldBeRed = true;
                if(B) {
                    String ContainedHeader = "";
                    int GroupIndex = NaN;
                    int index;
                    boolean noChildren = true;
                    for (int i = 0; i < adapter.getGroupCount(); ++i) {
                        if (s.toString().contains("/" + adapter.getGroup(i).toString())) {
                            if(!Exp.isGroupExpanded(i)){
                                Exp.expandGroup(i);
                            }
                            ContainedHeader = "/" + adapter.getGroup(i).toString();
                            GroupIndex = i;
                            break;
                        } else {
                            if ((Exp.isGroupExpanded(i))) {
                                Exp.collapseGroup(i);
                            }
                        }
                    }
                    if (GroupIndex != NaN) {

                        for (int j = 0; j < adapter.getChildrenCount(GroupIndex); ++j) {
                            String CompareString = ContainedHeader + "/" + adapter.getChild(GroupIndex, j).toString();
                            if (s.toString().equals(CompareString)) {
                                noChildren = false;
                                index = Exp.getFlatListPosition(Exp.getPackedPositionForChild(GroupIndex, j));
                                View v = (View) Exp.getChildAt(index);
                                if (v != null) {

                                    if (currentmarkerad != null) {
                                        currentmarkerad.setBackgroundColor(Color.WHITE);
                                    }
                                    v.setBackgroundColor(Color.GRAY);
                                    currentmarkerad = v;
                                }
                                ShouldBeRed = false;
                            }
                            else if(CompareString.contains(s.toString())){
                                ShouldBeRed = false;
                            }
                        }
                    }
                    else{
                        ShouldBeRed = false;
                    }
                    if (noChildren) {
                        if (currentmarkerad != null) {
                            currentmarkerad.setBackgroundColor(Color.WHITE);
                            currentmarkerad = null;
                        }
                    }
                    if(ShouldBeRed){
                        TV.setBackgroundColor(Color.RED);
                    }
                    else{
                        TV.setBackgroundColor(Color.WHITE);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        Exp.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {


                if(currentmarkerad != null) {
                    currentmarkerad.setBackgroundColor(Color.WHITE);
                }
                v.setBackgroundColor(Color.GRAY);
                currentmarkerad = v;
                B = false;
                TV.setText("/" + listDataHeader.get(groupPosition) + "/" +listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition));
                B = true;
                return false;
            }
        });
        Exp.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                B = false;
                String temp = "/"+listDataHeader.get(groupPosition);
                String temp2 = TV.getText().toString();
                if(!temp2.equals(temp))
                    TV.setText("/"+listDataHeader.get(groupPosition));
                B = true;
            }
        });
        Exp.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                B = false;
                TV.setText("/");
                B = true;
            }
        });
    }


    private void prepData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("one");
        listDataHeader.add("two");
        listDataHeader.add("three");

        // Adding child data
        List<String> P1 = new ArrayList<String>();
        P1.add("childOne");
        P1.add("childTwo");
        P1.add("childThree");
        P1.add("childFour");

        List<String> P2 = new ArrayList<String>();
        P2.add("childOne");
        P2.add("childTwo");
        P2.add("childThree");
        P2.add("childFour");

        List<String> P3 = new ArrayList<String>();
        P3.add("childOne");
        P3.add("childTwo");
        P3.add("childThree");
        P3.add("childFour");

        listDataChild.put(listDataHeader.get(0), P1); // Header, Child data
        listDataChild.put(listDataHeader.get(1), P2);
        listDataChild.put(listDataHeader.get(2), P3);
    }
}
