package com.example.odavi.tddc73labb1;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout firstLayout = new LinearLayout(this);
        firstLayout.setOrientation(LinearLayout.VERTICAL);
        firstLayout.setGravity(Gravity.CENTER_HORIZONTAL);

        Button topBotton = new Button(this);
        topBotton.setText("Button");
        topBotton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));

        EditText ET1 = new EditText(this);
        ET1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        ET1.setHint("Hint");

        RatingBar RBar = new RatingBar(this);
        RBar.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));

        EditText ET2 = new EditText(this);
        ET2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        ET2.setHint("Hint");


        firstLayout.addView(topBotton);
        firstLayout.addView(ET1);
        firstLayout.addView(RBar);
        firstLayout.addView(ET2);

        // SecondPage
        LinearLayout secondLayout = new LinearLayout(this);
        secondLayout.setOrientation(LinearLayout.HORIZONTAL);
        secondLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));

        LinearLayout leftLayout = new LinearLayout(this);
        leftLayout.setOrientation(LinearLayout.VERTICAL);
        leftLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        leftLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT, 1f));

        LinearLayout rightLayout = new LinearLayout(this);
        rightLayout.setOrientation(LinearLayout.VERTICAL);
        rightLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        rightLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT, 10f));

        TextView TV1 = new TextView(this);
        TV1.setText("Namn");
        TV1.setGravity(Gravity.CENTER_VERTICAL);
        TV1.setTextSize(18);
        TV1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150));

        TextView TV2 = new TextView(this);
        TV2.setText("Lösenord");
        TV2.setGravity(Gravity.CENTER_VERTICAL);
        TV2.setTextSize(18);
        TV2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150));

        TextView TV3 = new TextView(this);
        TV3.setText("E-post");
        TV3.setGravity(Gravity.CENTER_VERTICAL);
        TV3.setTextSize(18);
        TV3.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150));

        TextView TV4 = new TextView(this);
        TV4.setText("Ålder");
        TV4.setGravity(Gravity.CENTER_VERTICAL);
        TV4.setTextSize(18);
        TV4.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150));

        EditText EditName = new EditText(this);
        EditName.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
        EditName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150));


        EditText EditPassword = new EditText(this);
        EditPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        EditPassword.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150));


        EditText EditEmail = new EditText(this);
        EditEmail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        EditEmail.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150));


        SeekBar SB = new SeekBar(this);
        SB.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150));


        leftLayout.addView(TV1);
        leftLayout.addView(TV2);
        leftLayout.addView(TV3);
        leftLayout.addView(TV4);
        rightLayout.addView(EditName);
        rightLayout.addView(EditPassword);
        rightLayout.addView(EditEmail);
        rightLayout.addView(SB);
        secondLayout.addView(leftLayout);
        secondLayout.addView(rightLayout);
        setContentView(secondLayout);
        // Third View

        LinearLayout thirdLayout = new LinearLayout(this);
        thirdLayout.setOrientation(LinearLayout.VERTICAL);
        thirdLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
        thirdLayout.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView TopText = new TextView(this);
        TopText.setText("Hur trivs du på liu?");
        TopText.setTextSize(18);
        TopText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));

        RadioGroup RG = new RadioGroup(this);
        RG.setGravity(Gravity.LEFT);
        RG.setOrientation(LinearLayout.HORIZONTAL);
        RG.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));

        CheckBox Check1 = new CheckBox(this);
        Check1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        Check1.setText("Bra");

        CheckBox Check2 = new CheckBox(this);
        Check2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        Check2.setText("Mycket bra");

        CheckBox Check3 = new CheckBox(this);
        Check3.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        Check3.setText("Jättebra");

        RG.addView(Check1);
        RG.addView(Check2);
        RG.addView(Check3);

        TextView SecondText = new TextView(this);
        SecondText.setText("Läser du på LITH?");
        SecondText.setTextSize(18);
        SecondText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));

        RadioGroup RG2 = new RadioGroup(this);
        RG2.setGravity(Gravity.LEFT);
        RG2.setOrientation(LinearLayout.HORIZONTAL);
        RG2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));

        CheckBox CheckY = new CheckBox(this);
        CheckY.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        CheckY.setText("Ja");

        CheckBox CheckN = new CheckBox(this);
        CheckN.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        CheckN.setText("Nej");

        RG2.addView(CheckY);
        RG2.addView(CheckN);

        ImageView IV = new ImageView(this);
        IV.setImageResource(R.drawable.liu);
        IV.setLayoutParams(new LinearLayout.LayoutParams(720,720));



        TextView ThirdText = new TextView(this);
        ThirdText.setText("Är Detta Lius logga?");
        ThirdText.setTextSize(18);
        ThirdText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));

        RadioGroup RG3 = new RadioGroup(this);
        RG3.setGravity(Gravity.LEFT);
        RG3.setOrientation(LinearLayout.HORIZONTAL);
        RG3.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));

        CheckBox CheckY2 = new CheckBox(this);
        CheckY2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        CheckY2.setText("Ja");

        CheckBox CheckN2 = new CheckBox(this);
        CheckN2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        CheckN2.setText("Nej");

        RG3.addView(CheckY2);
        RG3.addView(CheckN2);

        Button SubmitButton = new Button(this);
        SubmitButton.setText("Skicka in");
        SubmitButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));




        thirdLayout.addView(TopText);
        thirdLayout.addView(RG);
        thirdLayout.addView(SecondText);
        thirdLayout.addView(RG2);
        thirdLayout.addView(IV);
        thirdLayout.addView(ThirdText);
        thirdLayout.addView(RG3);
        thirdLayout.addView(SubmitButton);
        //setContentView(thirdLayout);
        //setContentView(R.layout.secondpage);
    }
}
